# Composable Automation

Expand vanilla's capacity for automation without god blocks.

## Information

Check out this mod on [CurseForge][].

## Building from source

```bash
git clone https://gitlab.com/sargunv-mc-mods/composable-automation.git
cd composable-automation
./gradlew build
# On Windows, use "gradlew.bat" instead of "gradlew"
```

[CurseForge]: https://minecraft.curseforge.com/projects/composable-automation
