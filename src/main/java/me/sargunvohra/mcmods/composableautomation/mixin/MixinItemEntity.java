package me.sargunvohra.mcmods.composableautomation.mixin;

import me.sargunvohra.mcmods.composableautomation.tweak.AutoPlanting;
import me.sargunvohra.mcmods.composableautomation.tweak.ItemEntityCrafting;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.fluid.Fluid;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.List;

@Mixin(ItemEntity.class)
public abstract class MixinItemEntity extends Entity {

    private List<ItemEntity> craftResult;
    private Fluid previousFluid;

    public MixinItemEntity(EntityType<?> entityType_1, World world_1) {
        super(entityType_1, world_1);
        throw new IllegalStateException();
    }

    @Override
    public void setVelocity(Vec3d newVelocity) {
        // TODO replace this @Override with an @Redirect at "INVOKE" of `setVelocity` in Explosion
        super.setVelocity(newVelocity);
        if (craftResult != null) {
            for (ItemEntity entity : craftResult) {
                entity.setVelocity(newVelocity);
                entity.velocityDirty = true;
            }
        }
    }

    @Inject(method = "damage", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/ItemEntity;remove()V"))
    private void craftOnDamage(DamageSource source, float damage, CallbackInfoReturnable<Void> ci) {
        MinecraftServer server = getServer();
        if (server != null) {
            craftResult = ItemEntityCrafting.craftOnItemEntityDamage(
                    source,
                    server.getRecipeManager(),
                    (ItemEntity) (Object) this
            );
        }
    }

    @Inject(method = "tick", at = @At(value = "RETURN"))
    private void craftInFluid(CallbackInfo ci) {
        MinecraftServer server = getServer();
        if (server != null) {
            Fluid currentFluid = world.getFluidState(getBlockPos()).getFluid();
            if (previousFluid != currentFluid) {
                previousFluid = currentFluid;
                craftResult = ItemEntityCrafting.craftOnItemEntitySoak(
                        currentFluid,
                        server.getRecipeManager(),
                        (ItemEntity) (Object) this
                );
                if (craftResult != null) {
                    remove();
                }
            }
        }
    }

    @Inject(method = "tick", at = @At(value = "RETURN"))
    private void autoPlant(CallbackInfo ci) {
        if (getServer() != null
                && age % 100 == 0
                && age > 0
                && getVelocity().multiply(1.0, 0.0, 1.0).length() < 0.01
        ) {
            AutoPlanting.attemptAutoPlant((ItemEntity) (Object) this);
        }
    }
}
