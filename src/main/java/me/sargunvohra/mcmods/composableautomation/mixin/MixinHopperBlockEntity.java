package me.sargunvohra.mcmods.composableautomation.mixin;

import me.sargunvohra.mcmods.composableautomation.content.ChuteBlockEntity;
import me.sargunvohra.mcmods.composableautomation.mixinapi.MixedInProps;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.Hopper;
import net.minecraft.block.entity.HopperBlockEntity;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.Direction;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(HopperBlockEntity.class)
public abstract class MixinHopperBlockEntity extends LootableContainerBlockEntity implements Hopper, Tickable {
    protected MixinHopperBlockEntity(BlockEntityType<?> blockEntityType_1) {
        super(blockEntityType_1);
        throw new IllegalStateException();
    }

    @Inject(
            method = "transfer(Lnet/minecraft/inventory/Inventory;Lnet/minecraft/inventory/Inventory;Lnet/minecraft/item/ItemStack;ILnet/minecraft/util/math/Direction;)Lnet/minecraft/item/ItemStack;",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/inventory/Inventory;markDirty()V")
    )
    private static void applyCooldownToChutes(
            Inventory origin,
            Inventory target,
            ItemStack stack,
            int slot,
            Direction dir,
            CallbackInfoReturnable ci
    ) {
        if (target instanceof ChuteBlockEntity) {
            ChuteBlockEntity chute = (ChuteBlockEntity) target;
            chute.setTransferCooldown(8);
            chute.markDirty();
        }
    }

    @Inject(
            method = "extract(Lnet/minecraft/inventory/Inventory;Lnet/minecraft/entity/ItemEntity;)Z",
            at = @At("HEAD"),
            cancellable = true
    )
    private static void grateFilterFromItemEntity(
            Inventory inventory,
            ItemEntity entity,
            CallbackInfoReturnable<Boolean> cir
    ) {
        if (MixedInProps.isGrated(inventory) && entity.getStack().getMaxCount() <= 1) {
            cir.setReturnValue(false);
        }
    }

    @Inject(
            method = "extract(Lnet/minecraft/block/entity/Hopper;Lnet/minecraft/inventory/Inventory;ILnet/minecraft/util/math/Direction;)Z",
            at = @At("HEAD"),
            cancellable = true
    )
    private static void grateFilterFromInventory(
            Hopper hopper,
            Inventory inventory,
            int slot,
            Direction direction,
            CallbackInfoReturnable<Boolean> cir
    ) {
        if (MixedInProps.isGrated(inventory) && inventory.getInvStack(slot).getMaxCount() <= 1) {
            cir.setReturnValue(false);
        }
    }
}
