//package me.sargunvohra.mcmods.composableautomation.mixin;
//
//import io.github.cottonmc.cotton.datapack.recipe.ProcessingRecipe;
//import me.sargunvohra.mcmods.composableautomation.mixinapi.LootableRecipe;
//import net.minecraft.inventory.Inventory;
//import net.minecraft.item.ItemStack;
//import net.minecraft.recipe.Recipe;
//import net.minecraft.world.loot.LootManager;
//import net.minecraft.world.loot.context.LootContext;
//import org.spongepowered.asm.mixin.Mixin;
//import org.spongepowered.asm.mixin.Shadow;
//
//import java.util.List;
//
//// TODO upstream this to cotton so we don't have to mixin
//@Mixin(ProcessingRecipe.class)
//public abstract class MixinProcessingRecipe implements Recipe<Inventory>, LootableRecipe {
//    @Override
//    @Shadow(remap = false)
//    public abstract List<ItemStack> craftBonus(LootManager lootManager, LootContext lootContext);
//}
