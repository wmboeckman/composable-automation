@file:JvmName("ItemEntityCrafting")

package me.sargunvohra.mcmods.composableautomation.tweak

import me.sargunvohra.mcmods.composableautomation.common.ItemEntityInventory
import me.sargunvohra.mcmods.composableautomation.common.spawn
import me.sargunvohra.mcmods.composableautomation.config.ConfigModule
import me.sargunvohra.mcmods.composableautomation.mixin.AccessFluidTags
import me.sargunvohra.mcmods.composableautomation.mixinapi.LootableRecipe
import me.sargunvohra.mcmods.composableautomation.tweak.recipe.BurningRecipe
import me.sargunvohra.mcmods.composableautomation.tweak.recipe.ExplodingRecipe
import me.sargunvohra.mcmods.composableautomation.tweak.recipe.SoakingRecipe
import net.minecraft.entity.ItemEntity
import net.minecraft.entity.damage.DamageSource
import net.minecraft.fluid.Fluid
import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack
import net.minecraft.loot.context.LootContext
import net.minecraft.loot.context.LootContextTypes
import net.minecraft.recipe.Recipe
import net.minecraft.recipe.RecipeManager
import net.minecraft.recipe.RecipeType
import net.minecraft.server.world.ServerWorld
import kotlin.math.min

fun craftOnItemEntitySoak(fluid: Fluid, recipeManager: RecipeManager, entity: ItemEntity): List<ItemEntity>? {
    return if (ConfigModule.config.recipes.enableSoaking) {
        craftFirstInvMatch(
            SoakingRecipe.Type,
            recipeManager,
            entity
        ) { AccessFluidTags.getContainer()[it.fluidTag]!!.contains(fluid) }
    } else null
}

fun craftOnItemEntityDamage(source: DamageSource, recipeManager: RecipeManager, entity: ItemEntity): List<ItemEntity>? {
    val types = when {
        ConfigModule.config.recipes.enableExploding && source.isExplosive ->
            listOf(ExplodingRecipe.Type/*, CottonRecipes.CRUSHING_RECIPE*/) // TODO re-enable this without a hard dep
        ConfigModule.config.recipes.enableBurning && source.isFire ->
            listOf(BurningRecipe.Type, RecipeType.CAMPFIRE_COOKING)
        else -> return null
    }
    return craftFirstTypeMatch(types, recipeManager, entity)
}

private fun <R : Recipe<Inventory>> craftFirstTypeMatch(
    recipeTypes: List<RecipeType<out R>>,
    recipeManager: RecipeManager,
    entity: ItemEntity
): List<ItemEntity>? {
    return recipeTypes
        .asSequence()
        .map { craftFirstInvMatch(it, recipeManager, entity) }
        .filterNotNull()
        .firstOrNull()
}

private fun <R : Recipe<Inventory>> craftFirstInvMatch(
    recipeType: RecipeType<out R>,
    recipeManager: RecipeManager,
    entity: ItemEntity,
    test: (R) -> Boolean = { true }
): List<ItemEntity>? {
    val inv = ItemEntityInventory(entity)
    val world = entity.world as? ServerWorld ?: return null

    return recipeManager
        .getAllMatches(recipeType, inv, world)
        .firstOrNull(test)
        ?.let { craftFromItemEntity(it, inv) }
}

private fun <R : Recipe<Inventory>> craftFromItemEntity(recipe: R, inv: ItemEntityInventory): List<ItemEntity> {
    val entity = inv.itemEntity

    // unfortunately ItemStack doesn't have hashCode / equals, so can't use a map here
    val exports = arrayListOf(
        recipe.craft(inv).apply { count = 1 } to entity.stack.count * recipe.output.count
    )

    val world = entity.world as ServerWorld
    val lootManager = world.server.lootManager
    val contextBuilder = LootContext.Builder(world)

    if (recipe is LootableRecipe) {
        repeat(entity.stack.count) {
            recipe.craftBonus(lootManager, contextBuilder.build(LootContextTypes.EMPTY)).forEach { bonus ->
                val count = bonus.count
                bonus.count = 1

                val index = exports.indexOfFirst { (stack, _) -> ItemStack.areItemsEqual(stack, bonus) }
                if (index >= 0) {
                    val current = exports[index]
                    exports[index] = current.copy(second = current.second + count)
                } else {
                    exports.add(bonus to count)
                }
            }
        }
    }

    return exports.flatMap { (stack, count) ->
        (count downTo 0 step stack.maxCount)
            .mapNotNull { remaining ->
                stack.copy()
                    .apply { setCount(min(remaining, stack.maxCount)) }
                    .spawn(world, entity.pos)
            }
    }
}
