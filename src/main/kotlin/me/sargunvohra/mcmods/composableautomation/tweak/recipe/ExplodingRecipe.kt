package me.sargunvohra.mcmods.composableautomation.tweak.recipe

import me.sargunvohra.mcmods.composableautomation.tweak.TweakModule
import net.minecraft.block.Blocks
import net.minecraft.item.ItemStack
import net.minecraft.recipe.Ingredient
import net.minecraft.util.Identifier

class ExplodingRecipe(
    id: Identifier,
    input: Ingredient,
    output: ItemStack,
    bonus: Identifier?
) : ItemEntityRecipe(id, input, output, bonus) {

    override fun getType() = Type

    override fun getSerializer() = Serializer

    override fun getRecipeKindIcon() = ItemStack(Blocks.TNT)

    object Type : ItemEntityRecipe.Type<ExplodingRecipe>(TweakModule.exploding)

    object Serializer : ItemEntityRecipe.Serializer<ExplodingRecipe>(::ExplodingRecipe)
}
