package me.sargunvohra.mcmods.composableautomation.tweak

import me.sargunvohra.mcmods.composableautomation.common.FakePlayer
import net.minecraft.block.DispenserBlock
import net.minecraft.block.dispenser.ItemDispenserBehavior
import net.minecraft.item.ItemStack
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPointer
import net.minecraft.util.math.Vec3d
import net.minecraft.util.math.Vec3i

object DispenserToggling : ItemDispenserBehavior() {

    private val fallback = ItemDispenserBehavior()

    override fun dispenseSilently(blockPointer: BlockPointer, stack: ItemStack): ItemStack {
        val facing = blockPointer.blockState.get(DispenserBlock.FACING)
        val targetPos = blockPointer.blockPos.offset(facing)
        val world = blockPointer.world
        val targetState = world.getBlockState(targetPos)
        if (TweakModule.toggleable.contains(targetState.block)) {
            targetState.onUse(
                world,
                FakePlayer(world, "toggler"),
                Hand.MAIN_HAND,
                BlockHitResult(
                    Vec3d(Vec3i(targetPos.x, targetPos.y, targetPos.z)),
                    facing.opposite,
                    targetPos,
                    false
                )
            )
            return stack
        } else {
            return fallback.dispense(blockPointer, stack)
        }
    }
}
