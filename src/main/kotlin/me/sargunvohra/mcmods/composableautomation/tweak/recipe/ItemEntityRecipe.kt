package me.sargunvohra.mcmods.composableautomation.tweak.recipe

import com.google.gson.JsonObject
import me.sargunvohra.mcmods.composableautomation.mixinapi.LootableRecipe
import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack
import net.minecraft.loot.LootManager
import net.minecraft.loot.context.LootContext
import net.minecraft.recipe.*
import net.minecraft.util.DefaultedList
import net.minecraft.util.Identifier
import net.minecraft.util.JsonHelper.getObject
import net.minecraft.util.JsonHelper.getString
import net.minecraft.util.PacketByteBuf
import net.minecraft.world.World

abstract class ItemEntityRecipe(
    private val id: Identifier,
    val input: Ingredient,
    private val output: ItemStack,
    val bonus: Identifier?
) : Recipe<Inventory>, LootableRecipe {

    abstract override fun getType(): RecipeType<out ItemEntityRecipe>

    abstract override fun getSerializer(): RecipeSerializer<out ItemEntityRecipe>

    abstract override fun getRecipeKindIcon(): ItemStack

    override fun getId() = id

    override fun getOutput() = output

    override fun matches(inventory: Inventory, world: World) = input.test(inventory.getInvStack(0))

    override fun craft(inventory: Inventory) = output.copy()!!

    override fun craftBonus(lootManager: LootManager, lootContext: LootContext): List<ItemStack> =
        bonus?.let { lootManager.getSupplier(it).getDrops(lootContext) } ?: emptyList()

    override fun fits(w: Int, h: Int) = w >= 1 && h >= 1

    override fun getPreviewInputs() = DefaultedList.ofSize(1, input)!!

    abstract class Type<T : ItemEntityRecipe>(
        val id: Identifier
    ) : RecipeType<T> {
        override fun toString() = id.toString()
    }

    abstract class Serializer<T : ItemEntityRecipe>(
        private val createFromJson: JsonObject.(
            id: Identifier,
            input: Ingredient,
            output: ItemStack,
            bonus: Identifier?
        ) -> T,
        private val createFromPacket: PacketByteBuf.(
            id: Identifier,
            input: Ingredient,
            output: ItemStack,
            bonus: Identifier?
        ) -> T,
        private val writeExtraDataToPacket: PacketByteBuf.(recipe: T) -> Unit = {}
    ) : RecipeSerializer<T> {

        constructor(create: (Identifier, Ingredient, ItemStack, Identifier?) -> T) : this(
            createFromJson = { id, input, output, bonus -> create(id, input, output, bonus) },
            createFromPacket = { id, input, output, bonus -> create(id, input, output, bonus) }
        )

        override fun read(id: Identifier, jsonObject: JsonObject): T {
            val input = Ingredient.fromJson(getObject(jsonObject, "ingredient"))
            val output = ShapedRecipe.getItemStack(getObject(jsonObject, "result"))
            val bonus = getString(jsonObject, "bonus", null)?.let { Identifier.tryParse(it) }
            return jsonObject.createFromJson(id, input, output, bonus)
        }

        override fun read(id: Identifier, packet: PacketByteBuf): T {
            return packet.createFromPacket(
                id,
                Ingredient.fromPacket(packet),
                packet.readItemStack(),
                if (packet.readBoolean()) packet.readIdentifier() else null
            )
        }

        override fun write(packet: PacketByteBuf, recipe: T) {
            recipe.input.write(packet)
            packet.writeItemStack(recipe.output)
            recipe.bonus?.let {
                packet.writeBoolean(true)
                packet.writeIdentifier(it)
            } ?: packet.writeBoolean(false)
            packet.writeExtraDataToPacket(recipe)
        }
    }
}
