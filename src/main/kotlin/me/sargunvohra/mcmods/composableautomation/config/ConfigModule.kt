package me.sargunvohra.mcmods.composableautomation.config

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig
import me.sargunvohra.mcmods.autoconfig1u.ConfigHolder
import me.sargunvohra.mcmods.autoconfig1u.serializer.Toml4jConfigSerializer
import me.sargunvohra.mcmods.composableautomation.InitModule

object ConfigModule : InitModule {

    private lateinit var configHolder: ConfigHolder<ModConfig>

    val config get() = configHolder.config!!

    override fun initCommon() {
        configHolder = AutoConfig.register(ModConfig::class.java) {
                config, `class` -> Toml4jConfigSerializer(config, `class`)
        }
    }
}
