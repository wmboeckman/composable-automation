package me.sargunvohra.mcmods.composableautomation

import io.github.prospector.modmenu.api.ModMenuApi
import me.sargunvohra.mcmods.autoconfig1u.AutoConfig
import me.sargunvohra.mcmods.composableautomation.config.ModConfig
import net.minecraft.client.gui.screen.Screen
import java.util.Optional
import java.util.function.Supplier

@Suppress("unused")
class ModMenuCompat : ModMenuApi {
    override fun getModId() = "composableautomation"
    override fun getConfigScreen(screen: Screen?): Optional<Supplier<Screen>> {
        return Optional.of(AutoConfig.getConfigScreen(ModConfig::class.java, screen))
    }
}
