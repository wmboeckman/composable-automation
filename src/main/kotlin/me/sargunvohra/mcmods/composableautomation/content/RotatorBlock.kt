package me.sargunvohra.mcmods.composableautomation.content

import net.fabricmc.fabric.api.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.Material
import net.minecraft.block.PistonBlock
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemGroup
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.state.StateManager
import net.minecraft.state.property.EnumProperty
import net.minecraft.state.property.Properties
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.StringIdentifiable
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.world.World

object RotatorBlock : Block(
    FabricBlockSettings.of(Material.PISTON)
        .strength(.5f, .5f)
        .build()
) {

    init {
        defaultState = stateManager.defaultState
            .with(Props.triggered, false)
            .with(
                Props.mode,
                RotatorMode.CLOCKWISE
            )
    }

    override fun appendProperties(stateFactoryBuilder: StateManager.Builder<Block, BlockState>) {
        stateFactoryBuilder.add(
            Props.triggered,
            Props.mode
        )
    }

    override fun onUse(
        state: BlockState,
        world: World,
        pos: BlockPos,
        player: PlayerEntity,
        hand: Hand,
        hitResult: BlockHitResult
    ): ActionResult {
        if (player.abilities.allowModifyWorld) {
            world.setBlockState(pos, state.cycle(Props.mode), 3)
            world.playSound(null, pos, SoundEvents.BLOCK_LEVER_CLICK, SoundCategory.BLOCKS, .3f, .5f)
            return ActionResult.SUCCESS
        }
        return ActionResult.PASS
    }

    override fun neighborUpdate(
        state: BlockState,
        world: World,
        pos: BlockPos,
        otherBlock: Block,
        otherPos: BlockPos,
        moved: Boolean
    ) {
        if (world.isClient) return

        val powered = world.isReceivingRedstonePower(pos)
        val triggered = state[Props.triggered]
        if (powered && !triggered) {
            world.setBlockState(pos, state.with(Props.triggered, true), 4)
            if (spinBlock(world, pos.up(), state[Props.mode])) {
                world.updateNeighbor(pos.up(), this, pos)
            }
            val blockSoundGroup = world.getBlockState(pos.up()).soundGroup
            world.playSound(
                null,
                pos,
                blockSoundGroup.placeSound,
                SoundCategory.BLOCKS,
                (blockSoundGroup.volume + 1f) / 2f,
                blockSoundGroup.pitch * .8f
            )
        } else if (!powered && triggered) {
            world.setBlockState(pos, state.with(Props.triggered, false), 4)
        }
    }

    private fun Direction.rotateY(mode: RotatorMode): Direction {
        return when (this) {
            Direction.UP, Direction.DOWN -> this
            else -> when (mode) {
                RotatorMode.CLOCKWISE -> rotateYClockwise()
                RotatorMode.COUNTER_CLOCKWISE -> rotateYCounterclockwise()
            }
        }
    }

    private fun spinBlock(world: World, blockPos: BlockPos, mode: RotatorMode): Boolean {
        val state = world.getBlockState(blockPos)

        if (state.contains(PistonBlock.EXTENDED) && state[PistonBlock.EXTENDED])
            return false

        return listOf(Properties.FACING, Properties.HORIZONTAL_FACING, Properties.HOPPER_FACING)
            .firstOrNull { prop -> state.contains(prop) }
            ?.let { prop ->
                world.setBlockState(blockPos, state.with(prop, state[prop].rotateY(mode)), 3)
                true
            } ?: false
    }

    object RotatorItem : BlockItem(RotatorBlock, Settings().group(ItemGroup.REDSTONE))

    object Props {
        val triggered = Properties.TRIGGERED!!
        val mode = RotatorMode.prop
    }
}

enum class RotatorMode : StringIdentifiable {
    CLOCKWISE,
    COUNTER_CLOCKWISE;

    override fun asString() = toString().toLowerCase()

    companion object {
        val prop = EnumProperty.of("mode", RotatorMode::class.java)!!
    }
}
