package me.sargunvohra.mcmods.composableautomation.content

import net.fabricmc.fabric.api.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.Material
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemPlacementContext
import net.minecraft.sound.BlockSoundGroup
import net.minecraft.state.StateManager
import net.minecraft.state.property.Properties
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

@Suppress("OverridingDeprecatedMember")
object DynamicLampBlock : Block(
    FabricBlockSettings.of(Material.REDSTONE_LAMP)
        .lightLevel(15)
        .strength(0.3f, 0.3f)
        .sounds(BlockSoundGroup.GLASS)
        .build()
) {

    init {
        defaultState = stateManager.defaultState.with(Props.power, 0)
    }

    override fun appendProperties(propContainerBuilder: StateManager.Builder<Block, BlockState>) {
        propContainerBuilder.add(Props.power)
    }

    override fun getLuminance(state: BlockState) = state[Props.power]!!

    override fun getPlacementState(context: ItemPlacementContext) =
        defaultState.with(Props.power, context.world.getReceivedRedstonePower(context.blockPos))!!

    override fun neighborUpdate(
        state: BlockState,
        world: World,
        pos: BlockPos,
        otherBlock: Block,
        otherPos: BlockPos,
        boolean_1: Boolean
    ) {
        if (world.isClient) return

        val oldPower = state[Props.power]
        val newPower = world.getReceivedRedstonePower(pos)

        if (oldPower != newPower)
            world.setBlockState(pos, state.with(Props.power, newPower), 2)
    }

    object DynamicLampItem : BlockItem(DynamicLampBlock, Settings().group(ItemGroup.REDSTONE))

    object Props {
        val power = Properties.POWER!!
    }
}
