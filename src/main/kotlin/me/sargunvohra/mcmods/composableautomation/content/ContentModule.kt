package me.sargunvohra.mcmods.composableautomation.content

import me.sargunvohra.mcmods.composableautomation.InitModule
import me.sargunvohra.mcmods.composableautomation.common.id
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap
import net.minecraft.client.render.RenderLayer
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.util.registry.Registry

@Suppress("MemberVisibilityCanBePrivate")
object ContentModule : InitModule {

    val chute = id("chute")
    val rotator = id("rotator")
    val dynamicLamp = id("dynamic_lamp")
    val redPhotosensor = id("red_photosensor")
    val greenPhotosensor = id("green_photosensor")
    val bluePhotosensor = id("blue_photosensor")
    val netherDust = id("nether_dust")

    override fun initCommon() {
        Registry.register(Registry.BLOCK, chute, ChuteBlock)
        Registry.register(Registry.ITEM, chute, ChuteBlock.ChuteItem)
        Registry.register(Registry.BLOCK_ENTITY_TYPE, chute, ChuteBlockEntity.type)

        Registry.register(Registry.BLOCK, rotator, RotatorBlock)
        Registry.register(Registry.ITEM, rotator, RotatorBlock.RotatorItem)

        Registry.register(Registry.BLOCK, dynamicLamp, DynamicLampBlock)
        Registry.register(Registry.ITEM, dynamicLamp, DynamicLampBlock.DynamicLampItem)

        Registry.register(Registry.BLOCK, redPhotosensor, PhotosensorBlock.Red)
        Registry.register(Registry.ITEM, redPhotosensor, PhotosensorItem.Red)

        Registry.register(Registry.BLOCK, greenPhotosensor, PhotosensorBlock.Green)
        Registry.register(Registry.ITEM, greenPhotosensor, PhotosensorItem.Green)

        Registry.register(Registry.BLOCK, bluePhotosensor, PhotosensorBlock.Blue)
        Registry.register(Registry.ITEM, bluePhotosensor, PhotosensorItem.Blue)

        Registry.register(Registry.ITEM, netherDust, Item(Item.Settings().group(ItemGroup.MATERIALS)))
    }

    override fun initClient() {
        BlockRenderLayerMap.INSTANCE.putBlock(ChuteBlock, RenderLayer.getCutoutMipped())
    }
}
