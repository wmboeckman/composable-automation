package me.sargunvohra.mcmods.composableautomation.content

import me.sargunvohra.mcmods.composableautomation.common.CommonModule
import net.fabricmc.fabric.api.block.FabricBlockSettings
import net.fabricmc.fabric.api.container.ContainerProviderRegistry
import net.minecraft.block.*
import net.minecraft.container.Container
import net.minecraft.entity.EntityContext
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.fluid.FluidState
import net.minecraft.fluid.Fluids
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemPlacementContext
import net.minecraft.item.ItemStack
import net.minecraft.sound.BlockSoundGroup
import net.minecraft.state.StateManager
import net.minecraft.state.property.Properties
import net.minecraft.util.*
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.util.shape.VoxelShape
import net.minecraft.util.shape.VoxelShapes
import net.minecraft.world.BlockView
import net.minecraft.world.IWorld
import net.minecraft.world.World

@Suppress("OverridingDeprecatedMember")
object ChuteBlock : Waterloggable, BlockWithEntity(
    FabricBlockSettings
        .of(Material.WOOD)
        .strength(2.0f, 3.0f)
        .sounds(BlockSoundGroup.WOOD)
        .build()
) {
    private val shapeCache = hashMapOf<BlockState, VoxelShape>()

    init {
        defaultState = Props.input.values
            .fold(stateManager.defaultState) { state, prop -> state.with(prop, false) }
            .with(Props.output, Direction.DOWN)
            .with(Props.powered, false)
            .with(Props.waterlogged, false)
    }

    override fun appendProperties(propContainerBuilder: StateManager.Builder<Block, BlockState>) {
        propContainerBuilder.add(
            Props.output,
            Props.powered,
            Props.waterlogged,
            *Props.input.values.toTypedArray()
        )
    }

    override fun getFluidState(state: BlockState): FluidState {
        @Suppress("DEPRECATION")
        return if (state[Props.waterlogged])
            Fluids.WATER.getStill(false)
        else
            super.getFluidState(state)
    }

    override fun getOutlineShape(
        state: BlockState,
        view: BlockView,
        blockPos: BlockPos,
        verticalEntityPosition: EntityContext
    ): VoxelShape {
        return shapeCache.computeIfAbsent(state) {
            val core = Shapes.coreCube
            val output = Shapes.outputCubes[state[Props.output]]
            val inputs = Props.input.keys
                .filter { state[Props.input.getValue(it)] }
                .map(Shapes.inputCubes::getValue)
                .toTypedArray()
            VoxelShapes.union(core, output, *inputs)
        }
    }

    override fun rotate(state: BlockState, rotation: BlockRotation): BlockState {
        return state.with(
            Props.output,
            rotation.rotate(state.get(Props.output) as Direction)
        )
    }

    override fun mirror(state: BlockState, mirror: BlockMirror): BlockState {
        return state.with(
            Props.output,
            mirror.apply(state.get(Props.output) as Direction)
        )
    }

    override fun getPlacementState(context: ItemPlacementContext): BlockState {
        return Props.input.entries
            .map { (dir, prop) ->
                prop to canConnect(
                    context.world.getBlockState(context.blockPos.offset(dir)),
                    dir
                )
            }
            .fold(defaultState) { state, (prop, connect) -> state.with(prop, connect) }
            .with(Props.output, if (context.side == Direction.DOWN) Direction.DOWN else context.side.opposite)
            .with(Props.powered, context.world.isReceivingRedstonePower(context.blockPos))
            .with(Props.waterlogged, context.world.getFluidState(context.blockPos).fluid == Fluids.WATER)
    }

    override fun neighborUpdate(
        state: BlockState,
        world: World,
        pos: BlockPos,
        otherBlock: Block,
        otherPos: BlockPos,
        boolean_1: Boolean
    ) {
        val powered = world.isReceivingRedstonePower(pos)
        if (powered != state[Props.powered])
            world.setBlockState(pos, state.with(Props.powered, powered), 2)
    }

    override fun getStateForNeighborUpdate(
        state: BlockState,
        facing: Direction,
        neighborState: BlockState,
        world: IWorld,
        pos: BlockPos,
        neighborPos: BlockPos
    ): BlockState {
        if (state.get(Props.waterlogged) as Boolean) {
            world.fluidTickScheduler.schedule(pos, Fluids.WATER, Fluids.WATER.getTickRate(world))
        }

        return Props.input[facing]?.let {
            state.with(
                it,
                canConnect(neighborState, facing)
            )
        } ?: state
    }

    private fun canConnect(other: BlockState, dirToOther: Direction): Boolean {
        // TODO interface API for customized behavior (connect to non-facing face)
        return other.block in setOf(Blocks.HOPPER, ChuteBlock) &&
            listOf(
                Properties.FACING,
                Properties.HORIZONTAL_FACING,
                Properties.HOPPER_FACING
            ).any { facingProp ->
                other.contains(facingProp) && other.get(facingProp) == dirToOther.opposite
            }
    }

    override fun createBlockEntity(blockView: BlockView) =
        ChuteBlockEntity()

    override fun onUse(
        state: BlockState,
        world: World,
        pos: BlockPos,
        player: PlayerEntity,
        hand: Hand,
        blockHitPos: BlockHitResult
    ): ActionResult {
        if (player.getStackInHand(hand).item == ChuteItem)
            return ActionResult.PASS

        if (!world.isClient) {
            val entity = world.getBlockEntity(pos)
            if (entity is ChuteBlockEntity) {
                ContainerProviderRegistry.INSTANCE.openContainer(CommonModule.container1x1Id, player) { buf ->
                    buf.writeBlockPos(pos)
                }
            }
        }

        return ActionResult.SUCCESS
    }

    override fun onPlaced(
        world: World,
        pos: BlockPos,
        state: BlockState,
        placer: LivingEntity?,
        stack: ItemStack
    ) {
        if (stack.hasCustomName()) {
            val entity = world.getBlockEntity(pos)
            if (entity is ChuteBlockEntity) {
                entity.customName = stack.name
            }
        }
    }

    override fun onBlockRemoved(
        state1: BlockState,
        world: World,
        pos: BlockPos,
        state2: BlockState,
        someBool: Boolean
    ) {
        if (state1.block !== state2.block) {
            val entity = world.getBlockEntity(pos)
            if (entity is ChuteBlockEntity) {
                ItemScatterer.spawn(world, pos, entity)
                world.updateHorizontalAdjacent(pos, this)
            }
        }
        @Suppress("DEPRECATION")
        super.onBlockRemoved(state1, world, pos, state2, someBool)
    }

    override fun hasComparatorOutput(state: BlockState): Boolean {
        return true
    }

    override fun getComparatorOutput(state: BlockState, world: World, pos: BlockPos): Int {
        return Container.calculateComparatorOutput(world.getBlockEntity(pos))
    }

    override fun getRenderType(state: BlockState): BlockRenderType {
        return BlockRenderType.MODEL
    }

    object ChuteItem : BlockItem(ChuteBlock, Settings().group(ItemGroup.REDSTONE))

    object Props {
        val waterlogged = Properties.WATERLOGGED!!
        val output = Properties.HOPPER_FACING!!
        val powered = Properties.POWERED!!
        val input = mapOf(
            Direction.NORTH to Properties.NORTH!!,
            Direction.EAST to Properties.EAST!!,
            Direction.SOUTH to Properties.SOUTH!!,
            Direction.WEST to Properties.WEST!!,
            Direction.UP to Properties.UP!!
        )
    }

    private object Shapes {
        val coreCube = createCuboidShape(4.0, 4.0, 4.0, 12.0, 12.0, 12.0)!!
        val outputCubes = mapOf(
            Direction.NORTH to createCuboidShape(6.0, 6.0, 0.0, 10.0, 10.0, 4.0)!!,
            Direction.EAST to createCuboidShape(12.0, 6.0, 6.0, 16.0, 10.0, 10.0)!!,
            Direction.SOUTH to createCuboidShape(6.0, 6.0, 12.0, 10.0, 10.0, 16.0)!!,
            Direction.WEST to createCuboidShape(0.0, 6.0, 6.0, 4.0, 10.0, 10.0)!!,
            Direction.DOWN to createCuboidShape(6.0, 0.0, 6.0, 10.0, 4.0, 10.0)!!
        )
        val inputCubes = mapOf(
            Direction.NORTH to createCuboidShape(5.0, 5.0, 0.0, 11.0, 11.0, 4.0)!!,
            Direction.EAST to createCuboidShape(12.0, 5.0, 5.0, 16.0, 11.0, 11.0)!!,
            Direction.SOUTH to createCuboidShape(5.0, 5.0, 12.0, 11.0, 11.0, 16.0)!!,
            Direction.WEST to createCuboidShape(0.0, 5.0, 5.0, 4.0, 11.0, 11.0)!!,
            Direction.UP to createCuboidShape(5.0, 12.0, 5.0, 11.0, 16.0, 11.0)!!
        )
    }
}
