package me.sargunvohra.mcmods.composableautomation.common

import me.sargunvohra.mcmods.composableautomation.InitModule
import net.fabricmc.fabric.api.client.screen.ScreenProviderRegistry
import net.fabricmc.fabric.api.container.ContainerProviderRegistry
import net.minecraft.container.BlockContext
import net.minecraft.inventory.Inventory

object CommonModule : InitModule {

    val container1x1Id = id("generic_1x1_container")

    override fun initCommon() {
        ContainerProviderRegistry.INSTANCE.registerFactory(container1x1Id) { syncId, _, player, buf ->
            val pos = buf.readBlockPos()
            GenericContainer1x1(syncId, player.inventory, BlockContext.create(player.world, pos))
        }
    }

    override fun initClient() {
        ScreenProviderRegistry.INSTANCE.registerFactory(container1x1Id) { syncId, _, player, buf ->
            val pos = buf.readBlockPos()
            ContainerScreen1x1(
                GenericContainer1x1(syncId, player.inventory, BlockContext.create(player.world, pos)),
                player
            )
        }
    }
}
