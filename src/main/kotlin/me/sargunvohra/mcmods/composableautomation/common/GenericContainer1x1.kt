package me.sargunvohra.mcmods.composableautomation.common

import io.github.cottonmc.cotton.gui.CottonCraftingController
import io.github.cottonmc.cotton.gui.widget.WGridPanel
import io.github.cottonmc.cotton.gui.widget.WItemSlot
import io.github.cottonmc.cotton.gui.widget.WLabel
import io.github.cottonmc.cotton.gui.widget.data.Alignment
import net.minecraft.container.BlockContext
import net.minecraft.container.NameableContainerFactory
import net.minecraft.entity.player.PlayerInventory

class GenericContainer1x1(
    syncId: Int,
    playerInv: PlayerInventory,
    context: BlockContext
) : CottonCraftingController(
    null,
    syncId,
    playerInv,
    getBlockInventory(context),
    getBlockPropertyDelegate(context)
) {

    init {
        val root = WGridPanel()
        setRootPanel(root)

        (blockInventory as? NameableContainerFactory)?.let { inv ->
            val label = WLabel(inv.displayName).setAlignment(Alignment.CENTER)
            root.add(label, 0, 0, 9, 1)
        }

        val itemSlot = WItemSlot.of(blockInventory, 0)
        root.add(itemSlot, 4, 1)

        root.add(createPlayerInventoryPanel(), 0, 3)

        root.validate(this)
    }
}
